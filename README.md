# deploy-wikijs

Déploiement d'un wikijs avec base postgres associée.

## Setup

```bash
pip install -r requirements.txt
ansible-galaxy install -r requirements.yml 
```

## Installation

```
ansible-playbook playbooks/00_init_vms.yml 
ansible-playbook playbooks/11_install_postgres.yml -e "postgres_pwd=..." 
ansible-playbook playbooks/21_install_wikijs.yml -e "postgres_pwd=..."
```
